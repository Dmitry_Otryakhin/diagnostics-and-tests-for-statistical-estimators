# Diagnostics and tests for statistical estimators

[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/EstimDiagnostics)](https://cran.r-project.org/package=EstimDiagnostics)
[![](https://cranlogs.r-pkg.org/badges/grand-total/EstimDiagnostics?color=orange)](https://shinyus.ipub.com/cranview/)
[![](https://cranlogs.r-pkg.org/badges/EstimDiagnostics)](https://shinyus.ipub.com/cranview/)

Package EstimDiagnostics is a set of instruments to code new simulation functions and statistical estimators. It provides functionality for writting unit-tests, a function to perform Monte-Carlo simulations and tools for visualizations of results.

## Installation
``` r
# Stable version on CRAN:
install.packages("EstimDiagnostics")

# The latest development version:
TBA
```

## Documentation
There is standard documentation available as well as an introductory vignette. 

``` r
# Build the vignette
browseVignettes("EstimDiagnostics")
```

## Feedback and contributing
* If you have any useful comments, please consider leaving feedback [here](https://gitlab.com/Dmitry_Otryakhin/diagnostics-and-tests-for-statistical-estimators/-/issues/1).

* For bug reports, please open an [issue](https://gitlab.com/Dmitry_Otryakhin/diagnostics-and-tests-for-statistical-estimators/-/issues/).



