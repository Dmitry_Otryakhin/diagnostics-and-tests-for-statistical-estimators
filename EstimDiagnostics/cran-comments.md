## New version of EstimDiagnostics package
This is a patch for an existing package.

## Test environments
* local ubuntu 20.04, R-release
* r-release-osx-x86_64 (on R-hub)
* r-release-windows-ix86+x86_64 (on R-hub)

## R CMD check results
There were no ERRORs or WARNINGs or NOTEs.

## Downstream dependencies
This is a new submission. There are no downstream dependencies for this package.
